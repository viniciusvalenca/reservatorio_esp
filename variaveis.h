////////////Reservatorio///////////
int litros;
int litrosMax;
int percentual = 0;
float volume;
boolean bombaArmada = true;
boolean bombaArmadaAtual = false;
boolean automatico = false;
boolean automaticoAtual = true;
int nivel;
int nivelAtual;
int nivelMin;
int nivelMax;
int flagMin;
int flagMax;
int sensor;
int sensorAtual;
///////////////////////////////////

////////VAR Autorizacao/////////
const char* autorizacao;
////////////////////////////////////

////////VARS FIREBASE PUSH/////////
HTTPClient http;
String  regId="";
boolean regIdLigado;
String serve = "AAAAPED4PD0:APA91bFur_BBGh5vsMBDQURN-th4--gfHbp3KNZtx4qPheqN1S2dZ1xT-NxWYyyDvIXj93nQq3HOQMlSOdZ4qke5p1aZpGGmrsa4_eeSOpzfw4PN8NuQSfhdy9LWQZeSMxHX6vnLhFSa";
///////////////////////////////////

////////VARS PRIMEIRAPASSAGEM//////
boolean firstRun = true;
///////////////////////////////////

////////VARS ESP8266Wifi.h/////////
char endIP[24];
char ssid[24];
///////////////////////////////////

////////VARS RESETPIN/////////
int buttonState = 0;
///////////////////////////////////

////////VAR WEBSERVER/////////
ESP8266WebServer server(80);
////////////////////////////////////

////////VAR OLED/////////
SSD1306  display(0x3c, OLED_SDA, OLED_SCK);
////////////////////////////////////

////////VARS Grafico/////////
int tensaoA = 0;
int tensaoB = 0;
int tensaoC = 0;
/////////////////////////////

//////////WIFI/////////////////////
boolean iconeWifi = true;
boolean iconeWifiAtual = false;
///////////////////////////////////

///////COORDENADAS DESENHOS///////
///////LIMPAR AREA//////////////
int xTamLimpar;
int yTamLimpar;
///////Controles////////
int xLimparCtrl;
int yLimparCtrl;
///////Bomba////////
int xLimparBomba;
int yLimparBomba;
///////Wifi////////
int xLimparWifi;
int yLimparWifi;
////////////BOMBA///////////////
int xBomba;
int yBomba;
////////CONTROLES///////////////
int xCtrl;
int yCtrl;
////////SINAL WIFI/////////////
int xWifi;
int yWifi;
//////STRING PERCENT////////////
int xPercent;
int yPercent;
int xTamPercent;
int yTamPercent;

int xLimparPercent = 70;
int yLimparPercent = 5;
    
//////STRING LITROS/////////////
int xLitros;
int yLitros;
int xTamLitros;
int yTamLitros;
//////STRING LIMPAR LITROS/////////////
int xLimparLitros = 89;
int yLimparLitros = 20;

 //////STRING VOLUME/////////////
int xVolume;
int yVolume;
//////////////////////////////

