/* Version */
#define AQUA_SUN   "v0.0.3"

/*
DESENHO RESERVATORIO
*/

#define xReservatorio 0
#define yReservatorio 0
#define xTamReservatorio 5
#define yTamReservatorio 30
#define baseReservatorio 60

/*
  DEFINE SENSOR
*/
#define regulagemMin 19
#define regulagemMax 580


/*
  DEFINE BOMBA
*/
#define PinBaseTransistorBomba 14

/*
DEFINE NIVEL*/
#define PinoNivel A0

/*
  DEFINE RESET PIN
*/
#define BUTTONPIN D4

/*
  DEFINE OLED:
  SCK = GPIO04 -> D2
  SDA = GPIO05 -> D1
*/
#define OLED_SCK D2
#define OLED_SDA D1

/* Hostname*/
#define HOSTNAME_DEFAULT "RESERVATORIO000"

/*Json buffer sizes*/
#define AUTO_JSON_SIZE (JSON_OBJECT_SIZE(1))
#define AUTORIZACAO_JSON_SIZE (JSON_OBJECT_SIZE(1))
#define BOMBA_JSON_SIZE (JSON_OBJECT_SIZE(1))
#define LITROS_JSON_SIZE (JSON_OBJECT_SIZE(1))
#define NIVEL_JSON_SIZE (JSON_OBJECT_SIZE(1))
#define NIVELALERTA_JSON_SIZE (JSON_OBJECT_SIZE(2))
#define REGID_JSON_SIZE (JSON_OBJECT_SIZE(2))
#define VOLUME_JSON_SIZE (JSON_OBJECT_SIZE(1))


