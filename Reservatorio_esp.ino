#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <ESP8266HTTPClient.h>
#include <Wire.h>
#include "SSD1306.h"
#include <WiFiManager.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include "config.h"
#include "variaveis.h"
#include "displayOledSSD1306.h"
#include "images.h"
#include "desenhos.h"
#include "mensagens.h"
#include "firebase.h"

void configModeCallback (WiFiManager *myWiFiManager) {
  Serial.println("Entered config mode");
  Serial.println(WiFi.softAPIP());
  //if you used auto generated SSID, print it
  Serial.println(myWiFiManager->getConfigPortalSSID());
  setupCoordenadasOled();
  setupOled();
  mensagemNenhumaRedeEncontrada();
  delay(4000);
  mensagemModoReconfiguracao();
}

void setup() {
  Serial.begin(115200);
  //WiFiManager
  //Local intialization. Once its business is done, there is no need to keep it around
  WiFiManager wifiManager;
  WiFi.hostname(HOSTNAME_DEFAULT);
  //reset settings - for testing
  //wifiManager.resetSettings();

  //set callback that gets called when connecting to previous WiFi fails, and enters Access Point mode
  wifiManager.setAPCallback(configModeCallback);

  //fetches ssid and pass and tries to connect
  //if it does not connect it starts an access point with the specified name
  //here  "AutoConnectAP"
  //and goes into a blocking loop awaiting configuration
  if (!wifiManager.autoConnect(HOSTNAME_DEFAULT)) {
    Serial.println("failed to connect and hit timeout");
    //reset and try again, or maybe put it to deep sleep
    ESP.reset();
    delay(1000);
  }

  //if you get here you have connected to the WiFi
  Serial.println("conectado...");
  if (!MDNS.begin(HOSTNAME_DEFAULT)) {
    Serial.println("Error setting up MDNS responder!");
    while (1) {
      delay(1000);
    }
  }

  Serial.println("mDNS responder started");
  setupPersistencia();
  setupOled();
  setupCoordenadasOled();
  mensagemIniciarWLAN();
  mensagemEnderecoIP();
  delay(4000);
  display.clear();

  MDNS.addService("http", "tcp", 80);

  Serial.printf("\n\n");

  pinMode(BUTTONPIN, INPUT_PULLUP);

  delay(2000);

  desenharReservatorio(xReservatorio, yReservatorio, xTamReservatorio, yTamReservatorio, baseReservatorio);
  //desenharGota(xBomba, yBomba);
  desenharAuto(xCtrl, yCtrl);
  desenharSinalWIFI(xWifi, yWifi);

}

void loop() {
  if (firstRun) {
    if (litrosMax > 0) {
      mensagemMaxLitros(litrosMax);
      firstRun = false;
    }
  }

  buttonState = digitalRead(BUTTONPIN);
  //If button pressed...
  if (buttonState == LOW) {
    Serial.println("Reset acionado");
    resetDefaults();
  }

  if ( WiFi.status() == 1 ) {
    iconeWifi = false;
    Serial.println("Wifi Desconectado");
  } else {
    iconeWifi = true;
    Serial.println("Wifi Conectado");
  }

  Serial.print("WL_CONNECTED: ");
  Serial.println(WiFi.status());

  if (bombaArmada != bombaArmadaAtual) {
    if (!bombaArmada) {
      digitalWrite(PinBaseTransistorBomba, HIGH);
    } else {
      digitalWrite(PinBaseTransistorBomba, LOW);
    }
  }

  atualizarControles();

  lerNivelReservatorio();

  alerta();

  server.handleClient();
}

void lerNivelReservatorio() {

  sensor = analogRead(PinoNivel);
  Serial.print("Sensor analogico: ");
  Serial.println(sensor);
  if (sensor >= regulagemMin && sensor <= regulagemMax) {
    //Serial.print("Sensor analogico: ");
    //Serial.println(sensor);
    Serial.println();
    sensor = map(sensor, regulagemMin, regulagemMax, 1, 30);
    Serial.print("Sensor grafico: ");
    Serial.println(sensor);
  } else if (sensor < regulagemMin) {
    sensor = 0;
  } else if (sensor > regulagemMax) {
    sensor = 30;
  }

  if (sensorAtual != sensor) {
    limparFluidoReservatorio(0, 0, 5, 30, 60);
    desenharFluidoReservatorio(0, 0, 5, sensor, 60);
    display.display();
    limparAreaString(70, 5, 40, 10);
    percentual = map(sensor, 0, 30, 0, 100);
    mensagemPercentual(percentual);

    litros = map(percentual, 0, 100, 0, litrosMax);
    Serial.print("Litros: ");
    Serial.println(litros);
    sensorAtual = sensor;

    limparAreaString(89, 20, 40, 10);
    mensagemLitros(litros);
  }
  delay(500);
}

void alerta() {
  if (regIdLigado == true) {
    Serial.print("Alerta() litros => ");
    Serial.println(litros);
    if (nivelMin > litros && flagMin == 0) {
      Serial.print("Alerta() nivel Min => ");
      Serial.println(nivelMin);
      doit("Alerta! Nivel minimo atingido!", String(litros) + "L");
      flagMin = 1;
    }
    if (nivelMin < litros) {
      flagMin = 0;
    }

    if (nivelMax < litros && flagMax == 0) {
      Serial.print("Alerta() nivel Max => ");
      Serial.println(nivelMax);
      doit("Alerta! Nivel maximo atingido!", String(litros) + "L");
      flagMax = 1;
    }
    if (nivelMax > litros) {
      flagMax = 0;
    }


    Serial.print("FlagMin => ");
    Serial.println(flagMin);
    Serial.print("FlagMax => ");
    Serial.println(flagMax);
  }
}

void atualizarControles() {
  ////////////BOMBA/////////////////////////
  if (bombaArmadaAtual != bombaArmada) {
    if (bombaArmada) {
      desenharGota(xBomba, yBomba);
      digitalWrite(PinBaseTransistorBomba, HIGH);
    } else {
      limparAreaControles(xBomba, yBomba, xTamLimpar, yTamLimpar);
      digitalWrite(PinBaseTransistorBomba, LOW);
    }
    bombaArmadaAtual = bombaArmada;
  }
  ////////////AUTOMATICO/////////////////////
  if (automaticoAtual != automatico) {
    if (automatico) {
      limparAreaControles(xCtrl, yCtrl, xTamLimpar, yTamLimpar);
      desenharAuto(xCtrl, yCtrl);
    } else {
      limparAreaControles(xCtrl, yCtrl, xTamLimpar, yTamLimpar);
      desenharControleManual(xCtrl, yCtrl);
    }
    automaticoAtual = automatico;
  }
  //////////NIVEL BOMBA////////////////////////////
  if (automatico) {
    if (litros < nivel) {
      bombaArmada = true;
    } else {
      bombaArmada = false;
    }
    if (bombaArmadaAtual != bombaArmada) {
      if (bombaArmada) {
        desenharGota(xBomba, yBomba);
        digitalWrite(PinBaseTransistorBomba, HIGH);
      } else {
        limparAreaControles(xBomba, yBomba, xTamLimpar, yTamLimpar);
        digitalWrite(PinBaseTransistorBomba, LOW);
      }
      bombaArmadaAtual = bombaArmada;
    }
  }
  ////////////WIFI///////////////////////////
  if (iconeWifiAtual != iconeWifi) {
    if (iconeWifi) {
      limparAreaControles(xWifi, yWifi, xTamLimpar, yTamLimpar);
      desenharSinalWIFI(xWifi, yWifi);
    } else {
      limparAreaControles(xWifi, yWifi, xTamLimpar, yTamLimpar);
      desenharNOSinalWIFI(xWifi, yWifi);
    }
    iconeWifiAtual = iconeWifi;
  }
}

/*########################RESET#########################*/
void resetDefaults() {
  delay(3000);
  WiFi.disconnect();
  gravarAuto("{\"auto\":false}");
  gravarAutorizacao("{\"autorizacao\":\"0\"}");
  gravarBomba("{\"bomba\":false}");
  gravarLitros("{\"litros\":9000}");
  gravarNivel("{\"nivel\":0}");
  gravarNivelAlerta("{\"nivelMin\": 0,\"nivelMax\": 300000}");
  gravarRegId("{\"regId\":\"\",\"regIdLigado\": false}");
  gravarVolume("{\"volume\":false}");
  ESP.reset();
}

