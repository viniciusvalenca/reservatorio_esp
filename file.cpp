
#include "config.h"
#include "file.h"
#include <FS.h>



bool writeStringToFile(String path, String& dataString) {
  SPIFFS.remove(path);
  File file = SPIFFS.open(path, "w");
  if (!file) {
    Serial.print("File open Error: ");
    Serial.println(path);
    return false;
  }
  file.print(dataString);
  Serial.print("File Size: ");
  Serial.println(file.size(), DEC);
  file.close();
  Serial.print("Backup successful: ");
  Serial.println(path);
  Serial.print("data: ");
  Serial.println(dataString);
  return true;
}

bool getStringFromFile(String path, String& dataString) {
  File file = SPIFFS.open(path, "r");
  if (!file) {
    Serial.print("File open Error: ");
    Serial.println(path);
    return false;
  }
  Serial.print("File Size: ");
  Serial.println(file.size(), DEC);
  file.setTimeout(1);
  dataString = "";
  while (file.available()) {
    dataString += file.readString();
  }
  file.close();
  Serial.print("Restore successful: ");
  Serial.println(path);
  Serial.print("data: ");
  Serial.println(dataString);
  return true;
}

bool removeFile(String path) {
  Serial.println("Removed: " + path);
  return SPIFFS.remove(path);
}


