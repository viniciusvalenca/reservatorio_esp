#include <SPI.h>
#include <FS.h>
#include "file.h"
#include "webServer.h"
#include <ArduinoJson.h>

void setupPersistencia() {
  pinMode(PinBaseTransistorBomba, OUTPUT);
  pinMode(PinoNivel, INPUT);

  Serial.print("Initializing FS...");
  // prepare SPIFFS
  SPIFFS.begin();

  //presetsHashMap["presetManual"] = presetManual;

  Serial.println("Ler Config");

  lerAuto();
  lerBomba();
  lerLitros();
  lerNivel();
  lerNivelAlerta();
  lerRegId();
  //lerVolume();

  setupWebServer();
  //recarregar();
}

/*
  void recarregar() {
  if (!modoSetup) {
  } else {
    Serial.println("Entrou no modo setup");
    //atualizarOled();
  }
  }
*/

///////////////////////Automatico///////////////////////
void lerAuto() {
  DynamicJsonBuffer jsonBuffer(AUTO_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/auto.json", json)) {
    Serial.println(json);
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      Serial.println("parseObject() failed");
      return;
    }
    automatico = root["auto"];
    // Print values.
    Serial.print("Lendo Automatico: ");
    Serial.println(automatico);
  } else {
    Serial.println("error opening auto.json");
  }
  jsonBuffer.clear();
}

String lerAutoJson() {
  DynamicJsonBuffer jsonBuffer(AUTO_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/auto.json", json)) {
    Serial.println(json);
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      Serial.println("parseObject() failed");
    }
    Serial.print("Lendo Automatico JSON: ");
    Serial.println(json);
  } else {
    Serial.println("error opening auto.json");
  }
  jsonBuffer.clear();
  return json;
}

boolean gravarAuto(String json) {
  DynamicJsonBuffer jsonBuffer(AUTO_JSON_SIZE);
  yield();
  JsonObject& root = jsonBuffer.parseObject(json);
  if (!root.success()) {
    Serial.println("parseObject() failed");
    return false;
  }
  automatico = root["auto"];
  Serial.print("Gravando Automatico: ");
  Serial.println(automatico);
  boolean resposta = writeStringToFile("/auto.json", json);
  Serial.println(json);
  Serial.println("Writing done.");
  jsonBuffer.clear();
  return resposta;
}

///////////////////////FIM Automatico///////////////////////
//////////////////////Autorizacao//////////////////////////
void lerAutorizacao() {
  DynamicJsonBuffer jsonBuffer(AUTORIZACAO_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/autorizacao.json", json)) {
    Serial.println(json);
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      Serial.println("parseObject() failed");
      return;
    }
    autorizacao = root["autorizacao"];
    // Print values.
    Serial.println(autorizacao);
  } else {
    Serial.println("error opening autorizacao.json");
  }
  jsonBuffer.clear();
}

String lerAutorizacaoJson() {
  DynamicJsonBuffer jsonBuffer(AUTORIZACAO_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/autorizacao.json", json)) {
    Serial.println(json);
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      Serial.println("parseObject() failed");
    }
    Serial.println(autorizacao);
  } else {
    Serial.println("error opening autorizacao.json");
  }
  jsonBuffer.clear();
  return json;
}

boolean gravarAutorizacao(String json) {
  DynamicJsonBuffer jsonBuffer(AUTORIZACAO_JSON_SIZE);
  yield();
  JsonObject& root = jsonBuffer.parseObject(json);
  if (!root.success()) {
    Serial.println("parseObject() failed");
    return false;
  }
  autorizacao = root["autorizacao"];
  Serial.println(autorizacao);
  boolean resposta = writeStringToFile("/autorizacao.json", json);
  Serial.println(json);
  Serial.println("Writing done.");
  jsonBuffer.clear();
  return resposta;
}
/////////////////////FIM Autorizacao///////////////////////
///////////////////////Bomba///////////////////////
void lerBomba() {
  DynamicJsonBuffer jsonBuffer(BOMBA_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/bomba.json", json)) {
    Serial.println(json);
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      Serial.println("parseObject() failed");
      return;
    }
    bombaArmada = root["bomba"];
    // Print values.
    Serial.print("Lendo Bomba Ativada: ");
    Serial.println(bombaArmada);
  } else {
    Serial.println("error opening bomba.json");
  }
  jsonBuffer.clear();
}

String lerBombaJson() {
  DynamicJsonBuffer jsonBuffer(BOMBA_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/bomba.json", json)) {
    Serial.println(json);
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      Serial.println("parseObject() failed");
    }
    Serial.print("Bomba Armada JSON: ");
    Serial.println(json);
  } else {
    Serial.println("error opening bomba.json");
  }
  jsonBuffer.clear();
  return json;
}

boolean gravarBomba(String json) {
  DynamicJsonBuffer jsonBuffer(BOMBA_JSON_SIZE);
  yield();
  JsonObject& root = jsonBuffer.parseObject(json);
  if (!root.success()) {
    Serial.println("parseObject() failed");
    return false;
  }
  bombaArmada = root["bomba"];
  Serial.print("Gravando BombaArmada: ");
  Serial.println(bombaArmada);
  boolean resposta = writeStringToFile("/bomba.json", json);

  Serial.println(json);
  Serial.println("Writing done.");
  jsonBuffer.clear();
  return resposta;
}
///////////////////////FIM Bomba/////////////////////
///////////////////////Litros///////////////////////
void lerLitros() {
  DynamicJsonBuffer jsonBuffer(LITROS_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/litros.json", json)) {
    Serial.println(json);
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      Serial.println("parseObject() failed");
      return;
    }
    litrosMax = root["litros"];
    //limparAreaString(89, 35, 40, 10);
    //mensagemMaxLitros(litrosMax);
    Serial.print("Lendo Litros: ");
    Serial.println(litrosMax);
  } else {
    Serial.println("error opening litros.json");
  }
  jsonBuffer.clear();
}

String lerLitrosJson() {
  DynamicJsonBuffer jsonBuffer(LITROS_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/litros.json", json)) {
    Serial.println(json);
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      Serial.println("parseObject() failed");
    }
    Serial.print("Lendo Litros JSON: ");
    Serial.println(json);
  } else {
    Serial.println("error opening litros.json");
  }
  jsonBuffer.clear();
  return json;
}

boolean gravarLitros(String json) {
  DynamicJsonBuffer jsonBuffer(LITROS_JSON_SIZE);
  yield();
  JsonObject& root = jsonBuffer.parseObject(json);
  if (!root.success()) {
    Serial.println("parseObject() failed");
    return false;
  }
  litrosMax = root["litros"];
  litros = litrosMax;
  limparAreaString(89, 35, 40, 10);
  mensagemMaxLitros(litrosMax);
  Serial.print("Gravando Litros: ");
  Serial.println(litros);
  boolean resposta = writeStringToFile("/litros.json", json);

  Serial.println(json);
  Serial.println("Writing done.");
  jsonBuffer.clear();
  return resposta;
}
///////////////////////Nivel///////////////////////
void lerNivel() {
  DynamicJsonBuffer jsonBuffer(NIVEL_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/nivel.json", json)) {
    Serial.println(json);
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      Serial.println("parseObject() failed");
      return;
    }
    nivel = root["nivel"];
    // Print values.
    Serial.print("Lendo Nivel: ");
    Serial.println(nivel);
  } else {
    Serial.println("error opening nivel.json");
  }
  jsonBuffer.clear();
}

String lerNivelJson() {
  DynamicJsonBuffer jsonBuffer(NIVEL_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/nivel.json", json)) {
    Serial.println(json);
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      Serial.println("parseObject() failed");
    }
    // Print values.
    Serial.print("Lendo Nivel JSON: ");
    Serial.println(json);
  } else {
    Serial.println("error opening nivel.json");
  }
  jsonBuffer.clear();
  return json;
}

boolean gravarNivel(String json) {
  DynamicJsonBuffer jsonBuffer(NIVEL_JSON_SIZE);
  yield();
  JsonObject& root = jsonBuffer.parseObject(json);
  if (!root.success()) {
    Serial.println("parseObject() failed");
    return false;
  }
  nivel = root["nivel"];
  Serial.print("Gravando Nivel: ");
  Serial.println(nivel);
  boolean resposta = writeStringToFile("/nivel.json", json);

  Serial.println(json);
  Serial.println("Writing done.");
  jsonBuffer.clear();
}
///////////////////////FIM Nivel///////////////////////
///////////////////////Nivel Alerta////////////////////
void lerNivelAlerta() {
  DynamicJsonBuffer jsonBuffer(NIVELALERTA_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/nivelAlerta.json", json)) {
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      Serial.println("parseObject() failed");
      return;
    }

    nivelMin = root["nivelMin"];  //Implicit cast
    nivelMax = root["nivelMax"];  //Implicit cast
    Serial.print("Lendo NÃ­vel MÃ­nimo: ");
    Serial.println(nivelMin);
    Serial.print("Lendo NÃ­vel MÃ¡ximo: ");
    Serial.println(nivelMax);

  } else {
    Serial.println("error opening nivelAlerta.json");
  }
  jsonBuffer.clear();
}

String lerNivelAlertaJson() {
  DynamicJsonBuffer jsonBuffer(NIVELALERTA_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/nivelAlerta.json", json)) {
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      Serial.println("parseObject() failed");
    }
  } else {
    Serial.println("error opening nivelAlerta.json");
  }
  jsonBuffer.clear();
  return json;
}

boolean gravarNivelAlerta(String json) {
  DynamicJsonBuffer jsonBuffer(NIVELALERTA_JSON_SIZE);
  yield();
  JsonObject& root = jsonBuffer.parseObject(json);
  if (!root.success()) {
    Serial.println("parseObject() failed");
    return false;
  }

  nivelMin = root["nivelMin"];  //Implicit cast
  nivelMax = root["nivelMax"];  //Implicit cast
  Serial.print("Gravando Nivel Minimo: ");
  Serial.println(nivelMin);
  Serial.print("Gravando Nivel Maximo: ");
  Serial.println(nivelMax);

  Serial.println(json);
  boolean resposta = writeStringToFile("/nivelAlerta.json", json);
  Serial.println("Writing done.");
  jsonBuffer.clear();
  return resposta;
}
//////////////////////FIM Nivel Alerta/////////////////
/////////////////////RegID/////////////////////////////
void lerRegId() {
  DynamicJsonBuffer jsonBuffer(REGID_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/regId.json", json)) {
    Serial.println(json);
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      Serial.println("parseObject() failed");
      return;
    }
    regId = root["regId"].asString();
    regIdLigado = root["regIdLigado"];

    Serial.print("Lendo RegIDLigado: ");
    Serial.println(regIdLigado);
    Serial.print("Lendo RegID");
    Serial.println(regId);
  } else {
    Serial.println("error opening regId.json");
  }
  jsonBuffer.clear();
}

String lerRegIdJson() {
  DynamicJsonBuffer jsonBuffer(REGID_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/regId.json", json)) {
    Serial.println(json);
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      Serial.println("parseObject() failed");
    }
    Serial.print("Lendo RegIDLigado: ");
    Serial.println(regIdLigado);
    Serial.print("Lendo RegID: ");
    Serial.println(regId);
  } else {
    Serial.println("error opening regId.json");
  }
  jsonBuffer.clear();
  return json;
}

boolean gravarRegId(String json) {
  DynamicJsonBuffer jsonBuffer(REGID_JSON_SIZE);
  yield();
  JsonObject& root = jsonBuffer.parseObject(json);
  if (!root.success()) {
    Serial.println("parseObject() failed");
    return false;
  }
  regId = root["regId"].asString();
  regIdLigado = root["regIdLigado"];
  
  Serial.print("Lendo RegIDLigado: ");
  Serial.println(regIdLigado);
  Serial.print("Lendo RegID: ");
  Serial.println(regId);
  boolean resposta = writeStringToFile("/regId.json", json);

  Serial.println(json);
  Serial.println("Writing done.");
  jsonBuffer.clear();
  return resposta;
}
///////////////////FIM RegID///////////////////////////
///////////////////////Volume//////////////////////////
void lerVolume() {
  DynamicJsonBuffer jsonBuffer(VOLUME_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/volume.json", json)) {
    Serial.println(json);
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      Serial.println("parseObject() failed");
      return;
    }
    volume = root["volume"];
    // Print values.
    Serial.print("Lendo Volume: ");
    Serial.println(volume);
  } else {
    Serial.println("error opening volume.json");
  }
  jsonBuffer.clear();
}

String lerVolumeJson() {
  DynamicJsonBuffer jsonBuffer(VOLUME_JSON_SIZE);
  String json;
  yield();
  if (getStringFromFile("/volume.json", json)) {
    Serial.println(json);
    JsonObject& root = jsonBuffer.parseObject(json);
    if (!root.success()) {
      Serial.println("parseObject() failed");
    }
    // Print values.
    Serial.print("Lendo Volume JSON: ");
    Serial.println(json);
  } else {
    Serial.println("error opening volume.json");
  }
  jsonBuffer.clear();
  return json;
}

boolean gravarVolume (String json) {
  DynamicJsonBuffer jsonBuffer(VOLUME_JSON_SIZE);
  yield();
  JsonObject& root = jsonBuffer.parseObject(json);
  if (!root.success()) {
    Serial.println("parseObject() failed");
    return false;
  }
  volume = root["volume"];
  Serial.print("Gravando Volume: ");
  Serial.println(volume);

  boolean resposta = writeStringToFile("/volume.json", json);

  Serial.println(json);
  Serial.println("Writing done.");
  jsonBuffer.clear();
}
///////////////////////FIM Volume///////////////////////




