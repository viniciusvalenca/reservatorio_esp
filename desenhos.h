///////////////////////Desenhos/////////////////////////
void desenharImagemWifi() {
  // see http://blog.squix.org/2015/05/esp8266-nodemcu-how-to-create-xbm.html
  // on how to create xbm files
  display.drawXbm(34, 22, WiFi_Logo_width, WiFi_Logo_height, WiFi_Logo_bits);
}

void desenharAtencao(int x, int y) {
  display.drawXbm(x, y, atencao_peq_width, atencao_peq_height, atencao_peq_bits);
  display.display();
}

void desenharBarraAlerta() {
  display.drawXbm(5, 30, barraAlerta_width, barraAlerta_height, barraAlerta_bits);
  display.display();
}

void desenharImagemRedes() {
  display.drawXbm(44, 14, redes_width, redes_height, redes_bits);
  display.display();
}

void desenharImagemSinalWlan() {
  display.drawXbm(44, 20, sinalWlan_width, sinalWlan_height, sinalWlan_bits);
  display.display();
}

void limparAreaControles(int x, int y, int tamx, int tamy) {
  display.setColor(BLACK);
  display.fillRect(x, y, tamx, tamy);
  //display.fillRect(52, 13, 25, 25);
  display.display();
  display.setColor(WHITE);
}

void desenharSinalWIFI(int x, int y) {
  display.drawXbm(x, y, sinal_WIFI_width, sinal_WIFI_height, sinal_WIFI_bits);
  display.display();
}

void desenharNOSinalWIFI(int x, int y) {
  display.drawXbm(x, y, no_sinal_WIFI_width, no_sinal_WIFI_height, no_sinal_WIFI_bits);
  display.display();
}

void desenharGota(int x, int y) {
  display.drawXbm(x, y, gota_width, gota_height, gota_bits);
  display.display();
}

void desenharAuto(int x, int y) {
  display.drawXbm(x, y, auto_width, auto_height, auto_bits);
  display.display();
}

void desenharEnergia(int x, int y) {
  display.drawXbm(x, y, energia_width, energia_height, energia_bits);
  display.display();
}

void limparAreaString(int x, int y, int tamx, int tamy) {
  display.setColor(BLACK);
  display.fillRect(x, y, tamx, tamy);
  display.display();
  display.setColor(WHITE);
}

/*
void limparArestas(int x, int y, int tamx, int tamy, int tamBase) {
  display.setColor(BLACK);
  display.fillRect(x + (tamx * 2) -3, y + tamx + tamy + 1, tamBase - 2, 5);
  display.fillRect(x, y + 6, 3, 3);
  display.fillRect(x + tamBase + 10, y + 6, 6, 3);
  display.fillRect(x, y + (tamx * 3) + tamy + 1, tamBase + 10, 5);
  display.display();
  display.setColor(WHITE);
}
*/

void desenharControleManual(int x, int y) {
  display.drawXbm(x, y, manual_width, manual_height, manual_bits);
  display.display();
}

void desenharReservatorio(int x, int y, int tamx, int tamy, int tamBase) {
  //tampa
  display.drawRect(x, y, tamBase + 10, tamx);
  //parede esquerda
  display.drawRect(x + 5, y + tamx, tamx, tamy);
  //parede direita
  display.drawRect(x + tamBase, y + tamx, tamx, tamy);
  //base
  display.drawRect(x + 5, y + tamy + tamx, tamBase, tamx + 5);
}

void desenharFluidoReservatorio(int x, int y, int tamx, int tamy, int tamBase) {
  //fluido
  display.fillRect(x + (tamx * 2), y + tamx + (30 - tamy), tamBase - 10, tamy);
}

void limparFluidoReservatorio(int x, int y, int tamx, int tamy, int tamBase) {
  //fluido
  display.setColor(BLACK);
  display.fillRect(x + (tamx * 2), y + (tamx), tamBase - 10, tamy - 1);
  display.display();
  display.setColor(WHITE);
}

/////////////////////FIM Desenhos///////////////////////

