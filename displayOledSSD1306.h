void setupOled() {
  Serial.println("Testando OLED! (SSD1306 LIB)");
  display.init();
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.flipScreenVertically();
  display.setFont(ArialMT_Plain_10);
  display.drawString(0 , 0, "Testando OLED!");
  display.display();
  delay(3 * 1000);
  display.clear();
}

void setupCoordenadasOled() {
  ////////LIMPAR AREA/////////////
  xTamLimpar = 15;
  yTamLimpar = 15; 
  //////STRING LIMPAR PERCENT////////////
  xLimparPercent = 70;
  yLimparPercent = 5;
  xTamPercent = 40;
  yTamPercent = 10;
  //////STRING LIMPAR LITROS/////////////
  xLimparLitros = 89;
  yLimparLitros = 20;
  xTamLitros = 40;
  yTamLitros = 10;
  ////////CONTROLES///////////////
  xCtrl = 25;
  yCtrl = 48;
  ////////////BOMBA///////////////
  xBomba = 5;
  yBomba = 48;
  /////////SINAL WIFI/////////////
  xWifi = 45;
  yWifi = 48;
   //////STRING PERCENT////////////
  xPercent = 70;
  yPercent = 5;
  //////STRING LITROS/////////////
  xLitros = 89;
  yLitros = 20;
  //////STRING VOLUME/////////////
  xVolume = 89;
  yVolume = 20;
  ////////////////////////////////
}


