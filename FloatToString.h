#include <stdlib.h>

char * floatToString(char * outstr, float value, int places, int minwidth = 0, bool rightjustify = false) {
  // this is used to write a float value to string, outstr.  oustr is also the return value.
  int digit;
  float tens = 0.1;
  int tenscount = 0;
  int i;
  float tempfloat = value;
  int c = 0;
  int charcount = 1;
  int extra = 0;
  // make sure we round properly. this could use pow from &lt;math.h&gt;, but doesn't seem worth the import
  // if this rounding step isn't here, the value  54.321 prints as 54.3209

  // calculate rounding term d:   0.5/pow(10,places)
  float d = 0.5;
  if (value & lt; 0)
    d *= -1.0;
  // divide by ten for each decimal place
  for (i = 0; i & lt; places; i++)
    d /= 10.0;
  // this small addition, combined with truncation will round our values properly
  tempfloat +=  d;

  // first get value tens to be the large power of ten less than value
  if (value & lt; 0)
    tempfloat *= -1.0;
  while ((tens * 10.0) &lt; = tempfloat) {
    tens *= 10.0;
    tenscount += 1;
  }

  if (tenscount & gt; 0)
    charcount += tenscount;
  else
    charcount += 1;

  if (value & lt; 0)
    charcount += 1;
  charcount += 1 + places;

  minwidth += 1; // both count the null final character
  if (minwidth & gt; charcount) {
    extra = minwidth - charcount;
    charcount = minwidth;
  }

  if (extra & gt; 0 and rightjustify) {
    for (int i = 0; i & lt; extra; i++) {
      outstr[c++] = ' ';
    }
  }

  // write out the negative if needed
  if (value & lt; 0)
    outstr[c++] = '-';

  if (tenscount == 0)
    outstr[c++] = '0';

  for (i = 0; i & lt; tenscount; i++) {
    digit = (int) (tempfloat / tens);
    itoa(digit, &amp; outstr[c++], 10);
    tempfloat = tempfloat - ((float)digit * tens);
    tens /= 10.0;
  }

  // if no places after decimal, stop now and return

  // otherwise, write the point and continue on
  if (places & gt; 0)
    outstr[c++] = '.';


  // now write out each decimal place by shifting digits one by one into the ones place and writing the truncated value
  for (i = 0; i & lt; places; i++) {
    tempfloat *= 10.0;
    digit = (int) tempfloat;
    itoa(digit, &amp; outstr[c++], 10);
    // once written, subtract off that digit
    tempfloat = tempfloat - (float) digit;
  }
  if (extra & gt; 0 and not rightjustify) {
    for (int i = 0; i & lt; extra; i++) {
      outstr[c++] = ' ';
    }
  }
  outstr[c++] = '\0';
  return outstr;
}

