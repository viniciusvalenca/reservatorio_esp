//////////////AUTOMATICO/////////////////////////////////////////
void setAutoController() {
  if (server.hasArg("plain") == false) { //Check if body received
    server.send(200, "text/plain", "Body not received");
    return;
  }
  String message = "true";
  if ( gravarAuto(server.arg("plain"))) {
    message = "true";
  } else {
    message = "false";
  }
  server.send(200, "text/plain", message);
  Serial.println(message);
  //recarregar();
}

void getAutoController() {
  Serial.println("getAutoController() acessado");
  /*
  String json = lerAutoJson();
  server.send(200, "text/plain", json);
  Serial.println("GET Automatico: " + json);
  */
  String msg = String(automatico);  
  server.send(200, "text/plain", msg);
  Serial.println("GET Automaticoa: " + msg);
}
//////////////AUTORIZACAO///////////////////////////////////
void setAutorizacaoController() {
  if (server.hasArg("plain") == false) { //Check if body received
    server.send(200, "text/plain", "Body not received");
    return;
  }
  String message = "";
  if (gravarAutorizacao(server.arg("plain"))) {
    message = "true";
  } else {
    message = "false";
  }
  server.send(200, "text/plain", message);
  Serial.println(message);

  //recarregar();
}

void getAutorizacaoController() {
  String json = lerAutorizacaoJson();
  server.send(200, "text/plain", json);
  Serial.println("GET Autorizacao: " + json);
}
//////////////BOMBA/////////////////////////////////////////
void setBombaController() {
  if (server.hasArg("plain") == false) { //Check if body received
    server.send(200, "text/plain", "Body not received");
    return;
  }
  String message = "";
  if (gravarBomba(server.arg("plain"))) {
    message = "true";
  } else {
    message = "false";
  }
  server.send(200, "text/plain", message);
  Serial.println(message);

  //recarregar();
}

void getBombaController() {
  Serial.println("getBombaController() acessado");
  /*
  String json = lerBombaJson();  
  server.send(200, "text/plain", json);
  Serial.println("GET Bomba: " + json);
  */

  String msg = String(bombaArmada);  
  server.send(200, "text/plain", msg);
  Serial.println("GET Bomba: " + msg);
}
//////////////LITROS/////////////////////////////////////////
void setLitrosController() {
  if (server.hasArg("plain") == false) { //Check if body received
    server.send(200, "text/plain", "Body not received");
    return;
  }
  String message = "";
  if (gravarLitros(server.arg("plain"))) {
    message = "true";
  } else {
    message = "false";
  }
  server.send(200, "text/plain", message);
  Serial.println(message);

  //recarregar();
}

void getLitrosController() {
  Serial.println("getLitrosController() acessado");
  /*
  String json = lerLitrosJson();  
  server.send(200, "text/plain", json);
  Serial.println("GET Litros: " + json);
  */

  String msg = String(litrosMax);  
  server.send(200, "text/plain", msg);
  Serial.println("GET LitrosMAX: " + msg);
}
//////////////NIVEL/////////////////////////////////////////
void setNivelController() {
  if (server.hasArg("plain") == false) { //Check if body received
    server.send(200, "text/plain", "Body not received");
    Serial.println("falha");
    return;
  }
  String message = "";
  if (gravarNivel(server.arg("plain"))) {
    message = "true";
  } else {
    message = "false";
  }
  server.send(200, "text/plain", message);
  Serial.println("SET Nivel" + message);

  //recarregar();
}

void getNivelController() {
  Serial.println("getNivelController() acessado");
  /*
  String json = lerNivelJson();  
  server.send(200, "text/plain", json);
  Serial.println("GET Nivel: " + json);
  */
  //String msg = String(nivel);  
  server.send(200, "text/plain", (String)nivel);
  Serial.print("GET Nivel: ");
  Serial.println(nivel);
}
/////////////NIVEL ALERTA//////////////////////////////////////
void setNivelAlertaController() {
  if (server.hasArg("plain") == false) { //Check if body received
    server.send(200, "text/plain", "Body not received");
    return;
  }
  String message = "";
  if (gravarNivelAlerta(server.arg("plain"))) {
    message = "true";
  } else {
    message = "false";
  }
  server.send(200, "text/plain", message);
  Serial.println(message);

  //recarregar();
}

void getNivelAlertaController() {
  String json = lerNivelAlertaJson();  
  server.send(200, "text/plain", json);
  Serial.println("GET Alerta: " + json);
}
//////////////PERCENTUAL/////////////////////////////////////////
void getPercentualController() {
  Serial.println("getPercentualController() acessado");
  String message = (String)percentual;
  server.send(200, "text/plain", message);
  Serial.println(message);
}
//////////////REGID///////////////////////////////////////////
void setRegIdController() {
  if (server.hasArg("plain") == false) { //Check if body received
    server.send(200, "text/plain", "Body not received");
    return;
  }
  String message = "";
  if ( gravarRegId(server.arg("plain"))) {
    message = "true";
  } else {
    message = "false";
  }
  server.send(200, "text/plain", message);
  Serial.println(message);
}

void getRegIdController() {
  String json = lerRegIdJson();
  server.send(200, "text/plain", json);
  Serial.println("GET RegID: " + json);
}
//////////////VOLUME/////////////////////////////////////////
void setVolumeController() {
  if (server.hasArg("plain") == false) { //Check if body received
    server.send(200, "text/plain", "Body not received");
    Serial.println("falha");
    return;
  }
  String message = "";
  if (gravarVolume(server.arg("plain"))) {
    message = "true";
  } else {
    message = "false";
  }
  server.send(200, "text/plain", message);
  Serial.println(message);

  //recarregar();
}

void getVolumeController() {
  Serial.println("getVolumeController() acessado");
  String message = (String)nivel;
  server.send(200, "text/plain", message);
  Serial.println(message);
}
//////////////RESET/////////////////////////////////////////
void resetController() {
  server.send(200, "text/plain", "true");
  Serial.println("true");
  resetDefaults();
}

void setupWebServer() {
  Serial.println("Configurando WebServer!");
  //Iniciar o servidor
  server.on("/setAuto", HTTP_POST, setAutoController);
  server.on("/getAuto", HTTP_GET, getAutoController);
  server.on("/setAutorizacao", HTTP_POST, setAutorizacaoController);
  server.on("/getAutorizacao", HTTP_GET, getAutorizacaoController);
  server.on("/setBomba", HTTP_POST, setBombaController);
  server.on("/getBomba", HTTP_GET, getBombaController);
  server.on("/setLitros", HTTP_POST, setLitrosController);
  server.on("/getLitros", HTTP_GET, getLitrosController);
  server.on("/setNivel", HTTP_POST, setNivelController);
  server.on("/getNivel", HTTP_GET, getNivelController);
  server.on("/setNivelAlerta", HTTP_POST, setNivelAlertaController);
  server.on("/getNivelAlerta", HTTP_GET, getNivelAlertaController);
  server.on("/getPercentual", HTTP_GET, getPercentualController);
  server.on("/setRegId", HTTP_POST, setRegIdController);
  server.on("/getRegId", HTTP_GET, getRegIdController);
  server.on("/setVolume", HTTP_POST, setVolumeController);
  server.on("/getVolume", HTTP_GET, getVolumeController);
  server.on("/resetDefault", HTTP_GET, resetController);
  server.begin();
}




